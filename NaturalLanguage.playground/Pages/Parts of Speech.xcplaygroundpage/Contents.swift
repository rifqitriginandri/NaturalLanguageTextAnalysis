
import NaturalLanguage

let text = "Nama saya adalah rifqi こんにちは、私の名前はリフキ・トリギナンドリです."

let tagger = NLTagger(tagSchemes: [.lexicalClass, .language, .script])
tagger.string = text


tagger.enumerateTags(in: text.startIndex..<text.endIndex, unit: NLTokenUnit.word, scheme: NLTagScheme.language, options: [.omitPunctuation, .omitWhitespace]) { (tag, range) -> Bool in
    print(text[range])
    print(tag?.rawValue ?? "unknown")
    return true
}
