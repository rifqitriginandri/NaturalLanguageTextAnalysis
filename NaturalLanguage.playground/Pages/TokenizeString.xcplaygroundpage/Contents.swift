
import NaturalLanguage

let text = "こんにちは、私の名前はリフキ・トリギナンドリです. Saya sedang belajar machine learning iOS di Udemy"

let tagger = NLTagger(tagSchemes: [.tokenType])
tagger.string = text


tagger.enumerateTags(in: text.startIndex..<text.endIndex, unit: NLTokenUnit.word, scheme: NLTagScheme.tokenType, options: [.omitPunctuation, .omitWhitespace]) { (tag, range) -> Bool in
    print(text[range])
    return true
}
